﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CannonShot : MonoBehaviour {


	public GameObject cannonBallEmitter;


	public GameObject cannonBall;

	public float ballSpeed = 1000;

	public float cooldown = 1;
	private float cooldownValue;
    public float cooldownOnStart = 0;

	public AudioClip[] clips;
	private AudioSource audioSource; // important: add audiosource component to gameobject

	public GameObject burstPrefab;
	private GameObject instantiateObj;

	void Awake() {
		
	}

	// Use this for initialization
	void Start () {
		cooldownValue = cooldown;
		cooldown += cooldownOnStart;
		audioSource = FindObjectOfType<AudioSource> ();
	}
	
	// Update is called once per frame
	void Update () {
		if (cooldown < Time.time) { //This checks wether real time has caught up to the timer
			//The CannonBall instantiation happens here.
			GameObject TemporaryBallHandler;
			TemporaryBallHandler = Instantiate(cannonBall, cannonBallEmitter.transform.position, cannonBallEmitter.transform.rotation) as GameObject;

			//Retrieve the Rigidbody component from the instantiated Ball and control it.
			Rigidbody Temporary_RigidBody;
			Temporary_RigidBody = TemporaryBallHandler.GetComponent<Rigidbody>();

			Temporary_RigidBody.velocity = cannonBallEmitter.transform.forward * ballSpeed;

			instantiateObj = Instantiate(burstPrefab, transform.position, Quaternion.identity);
			instantiateObj.transform.position = cannonBallEmitter.transform.position;
			instantiateObj.transform.rotation = cannonBallEmitter.transform.rotation;
			Destroy (instantiateObj, 2.0f);

			if (clips.Length > 0) {
				audioSource.clip = getRandomClip ();
				audioSource.Play ();
			}


			//if (gameObject.transform.position.y < 0)
			//	Destroy(TemporaryBallHandler);

			cooldown = Time.time + 3; //This sets the timer 3 seconds into the future
		}
	}

	private AudioClip getRandomClip() {
		return clips[Random.Range(0, clips.Length)];
	}
}
