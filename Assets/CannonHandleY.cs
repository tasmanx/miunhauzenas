﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CannonHandleY : MonoBehaviour
{
    public Quaternion angleDifHeight;
    Transform contrLeft;
    Transform contrRight;
    public Transform hand;
    public bool vr;
    public bool touched;   //make graphics using this one
    public bool activated; //make graphics using this one
    public GameObject target;
    public float sensitivityX = 1f;
    Quaternion lastRotation;
    //y stuff
    public Quaternion angleDif;
    private SteamVR_Controller.Device ControllerLeft
    {
        get
        {
            if (vr)
                return SteamVR_Controller.Input((int)contrLeft.GetComponent<SteamVR_TrackedObject>().index);
            else return null;
        }
    }
    private SteamVR_Controller.Device ControllerRight
    {
        get
        {
            if (vr)
                return SteamVR_Controller.Input((int)contrRight.GetComponent<SteamVR_TrackedObject>().index);
            else return null;
        }
    }
    void Start() {
        target.transform.localPosition = new Vector3(target.transform.localPosition.x, 0, target.transform.localPosition.z);
    }
    void OnTriggerStay(Collider collider)
    {
        if (collider.gameObject.tag == "hand")
        {
            touched = true;
            hand = collider.transform;
        }
    }
    void OnTriggerExit(Collider collider)
    {
        if (collider.gameObject.tag == "hand")
        {
            touched = false;
        }
    }
    void Update()
    {
        if (activated)
        {
            TurnHandle();
            MoveTarget();
        }
        //upgrade:
        if (!vr)
        {
            if (Input.GetMouseButtonDown(0) && touched)
            {
                //SetAngleDifs();
                activated = true;
            }
            if (Input.GetMouseButtonUp(0))
            {
                //SetAngleDifs();
                activated = false;
            }
        }
        else
        {
            if (ControllerLeft.GetHairTriggerDown() && touched)
            {
                hand = contrLeft;
                activated = true;
            }
            if (ControllerRight.GetHairTriggerDown() && touched)
            {
                hand = contrRight;
                activated = true;
            }
            if ((ControllerLeft.GetHairTriggerUp() && hand == contrLeft) || (ControllerRight.GetHairTriggerUp() && hand == contrRight))
            {
                activated = false;
            }
        }
    }

    void TurnHandle()
    {
        lastRotation = transform.parent.rotation;
        Vector3 targetPos = hand.position - transform.parent.position;
        targetPos.y = 0;
        Quaternion targetRotation = Quaternion.LookRotation((targetPos).normalized, Vector3.up);
        targetRotation.eulerAngles = new Vector3(0, targetRotation.eulerAngles.y, -90);
        transform.parent.rotation = targetRotation;// * angleDifHeight;
    }
    void MoveTarget()
    {
        Vector3 awayFromCannon = -(transform.parent.parent.position - target.transform.position).normalized;
        //awayFromCannon.y = 0;
        Quaternion quatDif = Quaternion.Inverse(transform.parent.rotation) * lastRotation; // gives the increment from handle rotations
        awayFromCannon = Quaternion.Euler(0, 90, 0) * awayFromCannon; // rotates the away direction 90 degrees to move sideways.
        //Debug.Log(quatDif.x);
        target.transform.position += awayFromCannon * quatDif.x * sensitivityX;
    }
}
