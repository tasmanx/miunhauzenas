﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FadeManager : MonoBehaviour {


    public GameObject lightObject;

    private static float lightIntensity;
    private static float maxLight;
    private static Light light;
    public float range;
    private static bool fadeIn;
    private static bool fade;

	// Use this for initialization
	void Start () {
        light = lightObject.GetComponent<Light>();
        lightIntensity = 0;
        fadeIn = true;
        fade = true;
        //lightIntensity = light.intensity;
        maxLight = 0;
        lightObject.SetActive(true);
    }
	
	// Update is called once per frame
	void Update () {
        //Debug.Log(lightIntensity + "   " + light.intensity);
        if (fadeIn && fade) {
            lightIntensity += range;
        }
        if (!fadeIn && fade) {
            lightIntensity -= range;
        }
        //Debug.Log(fade + "  2  " + fadeIn + "   " + lightIntensity + "   " + maxLight);
        if (lightIntensity <= 0 || lightIntensity >= 1) {
            fade = false;
        }
        //Debug.Log(fade + "  1  " + fadeIn + "   " + lightIntensity + "   " + maxLight);
        //Debug.Log(fade + "    " + fadeIn);
        if (fade) {
            light.intensity = lightIntensity;
        }
	}

    public static void FadeIn() {
        Debug.Log("HELLO fade");
       // light = lightObject.GetComponent<Light>();
        fade = true;
        fadeIn = true;
        light.intensity = maxLight;

        lightIntensity = maxLight;
    }
    public static void FadeOut()
    {
        Debug.Log("HELLO fade");
        // light = lightObject.GetComponent<Light>();
        fade = true;
        fadeIn = false;
        light.intensity = 1;

        lightIntensity = 1;
    }

}
