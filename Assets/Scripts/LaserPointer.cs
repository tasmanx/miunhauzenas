﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LaserPointer : MonoBehaviour {

    private SteamVR_TrackedObject trackedObj;

    // Lazeris
    public GameObject laserPrefab;
    // kontroleriu pozicijoms
    public Transform cameraRigTransform;
    // Nuoroda i teleporto taikiklio prefab'a
    public GameObject teleportReticlePrefab;
    // Galvos kontroleris
    public Transform headTransform;
    // atstumas nuo grindu
    public Vector3 teleportReticleOffset;


    // nuoroda i lazerio reiksme
    private GameObject laser;
    // Kintamasis saugo ir manipuliuoja laser objekto transformacija.
    private Transform laserTransform;
    // lazerio pataikymo pozicija
    private Vector3 hitPoint;
    private GameObject reticle;
    // Stores a reference to the teleport reticle transform for ease of use.
    private Transform teleportReticleTransform;
    // Is a layer mask to filter the areas on which teleports are allowed.
    public LayerMask teleportMask;
    // Is set to true when a valid teleport location is found.
    private bool shouldTeleport;

    private SteamVR_Controller.Device Controller
    {
        get { return SteamVR_Controller.Input((int)trackedObj.index); }
    }

    void Awake()
    {
        trackedObj = GetComponent<SteamVR_TrackedObject>();
    }


    // Use this for initialization
    void Start () {
        // Perkelia lazeri is resursu folderio i hierarchija
        laser = Instantiate(laserPrefab);
        // priskiriam transformacijos kintamajam kad lengviau butu manipuliuoti lazerio transformacijomis
        laserTransform = laser.transform;

        // Inicijuojamas taikinys
        reticle = Instantiate(teleportReticlePrefab);
        // taikinio transformaciijos priskiriamos "Transform" kintamajam.
        teleportReticleTransform = reticle.transform;
    }
	
	// Update is called once per frame
	void Update () {
        // Jai nuspaudziam touchpada

        Debug.Log("Is in air:  " + ColliderDebug.isInAir);
		if (Controller.GetPress(SteamVR_Controller.ButtonMask.Touchpad) && !grabHandller.flying)
        {
            RaycastHit hit;

            // spindulys einantis is kontrolerio
            if (Physics.Raycast(trackedObj.transform.position, transform.forward, out hit, 10, teleportMask))
            {
                hitPoint = hit.point;
                ShowLaser(hit);
                // Parodo teleportavimosi tinkleli
                reticle.SetActive(true);
                // perkeliam i tinklelio vieta
                teleportReticleTransform.position = hitPoint + teleportReticleOffset;
                // rasta vieta kur galima teleportuotis
                shouldTeleport = true;
            }
            else {
                laser.SetActive(false);
                reticle.SetActive(false);
                shouldTeleport = false;
            }
        }
        else // nerodom lazerio kai zaidejas atleidzia touchpad'q
        {
            laser.SetActive(false);
            reticle.SetActive(false);// slepiam tinkleli ( taikini ) jai i ta vieta negalima teleportuotis.
        }
        if (Controller.GetPressUp(SteamVR_Controller.ButtonMask.Touchpad) && shouldTeleport)
        {
            Teleport();
        }

    }

    private void ShowLaser(RaycastHit hit)
    {
        // rodyti lazeri
        laser.SetActive(true);
        // Pozicija tarp kontrolerio ir raycast susikirtimo tasko, 0.5f - keliauja iki puseukelio ( 50 % )
        laserTransform.position = Vector3.Lerp(trackedObj.transform.position, hitPoint, .5f);
        // nutaikyti lazeri i raycast taska
        laserTransform.LookAt(hitPoint);
        // skailina lazeri
        laserTransform.localScale = new Vector3(laserTransform.localScale.x, laserTransform.localScale.y,
            hit.distance);
    }

    // valdo teleportavimasi.
    private void Teleport()
    {
        // Neleidzia pradet naujos teleportacijos kol vykdoma senoji.
        shouldTeleport = false;
        // Slepia tinkleli
        reticle.SetActive(false);
        // atstumas tarp cameraRig'o ir zaidejo galvos
        Vector3 difference = cameraRigTransform.position - headTransform.position;
        // nustatom y i 0
        difference.y = 0;
        // Judam i hit point'a
        cameraRigTransform.position = hitPoint + difference;
		cameraRigTransform.GetComponent<Rigidbody> ().velocity = Vector3.zero;
    }

}
