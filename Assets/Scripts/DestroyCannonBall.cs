﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyCannonBall : MonoBehaviour {

	public GameObject prefab;
	private GameObject instantiateObj;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void OnCollisionEnter(Collision collision) {
//		Debug.Log("collision");
		if (collision.gameObject.tag == "Terrain") {
			//Debug.Log("terrain");
			instantiateObj = Instantiate(prefab, transform.position, Quaternion.identity);
			Destroy (instantiateObj, 2.0f);
			Destroy (gameObject);
		}
	}

}
