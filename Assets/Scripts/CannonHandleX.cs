﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CannonHandleX : MonoBehaviour
{
    public Quaternion angleDifHeight;
    Transform contrLeft;
    Transform contrRight;
    public Transform hand;
    public bool vr;
    public bool touched;   //make graphics using this one
    public bool activated;//make graphics using this one
    public GameObject target;
    public float sensitivityX = 1f;
    Quaternion lastRotation;
    public float targetClosestDistance = 2f;
    private SteamVR_Controller.Device ControllerLeft
    {
        get
        {
            if (vr)
                return SteamVR_Controller.Input((int)contrLeft.GetComponent<SteamVR_TrackedObject>().index);
            else return null;
        }
    }
    private SteamVR_Controller.Device ControllerRight
    {
        get
        {
            if (vr)
                return SteamVR_Controller.Input((int)contrRight.GetComponent<SteamVR_TrackedObject>().index);
            else return null;
        }
    }
    void OnTriggerStay(Collider collider)
    {
        if (collider.gameObject.tag == "hand")
        {
            touched = true;
            hand = collider.transform;
        }
    }
    void OnTriggerExit(Collider collider)
    {
        if (collider.gameObject.tag == "hand")
        {
            touched = false;
        }
    }
        void Update()
    {
        if (activated)
        {
            TurnHandle();
            MoveTarget();
        }
        //upgrade:
        if (!vr)
        {
            if (Input.GetMouseButtonDown(0) && touched)
            {
                //SetAngleDifs();
                activated = true;
            }
            if (Input.GetMouseButtonUp(0))
            {
                //SetAngleDifs();
                activated = false;
            }
        }
        else
        {
            if (ControllerLeft.GetHairTriggerDown() && touched)
            {
                hand = contrLeft;
                activated = true;
            }
            if (ControllerRight.GetHairTriggerDown() && touched)
            {
                hand = contrRight;
                activated = true;
            }
            if ((ControllerLeft.GetHairTriggerUp() && hand == contrLeft) || (ControllerRight.GetHairTriggerUp() && hand == contrRight)) {
                activated = false;
            }
        }
    }

    void TurnHandle()
    {
        lastRotation = transform.parent.rotation;
        Vector3 targetPos = hand.position - transform.parent.position;
        targetPos.x = 0;
        Quaternion targetRotation = Quaternion.LookRotation((targetPos).normalized, Vector3.right);
        targetRotation.y = 0;
        targetRotation.z = 0;
        transform.parent.rotation = targetRotation;// * angleDifHeight;
    }
    void MoveTarget() {
        Vector3 awayFromCannon = transform.parent.parent.position - target.transform.position;
        Quaternion quatDif = Quaternion.Inverse(transform.parent.rotation) * lastRotation;
        if(((target.transform.position + awayFromCannon.normalized * quatDif.x * sensitivityX) - transform.parent.parent.position).magnitude > targetClosestDistance)
            target.transform.position += awayFromCannon.normalized * quatDif.x * sensitivityX;
    }
}
