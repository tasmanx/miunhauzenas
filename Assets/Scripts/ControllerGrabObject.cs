﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControllerGrabObject : MonoBehaviour {
    // kolideris 
    private GameObject collidingObject;
    // is esmes grabed object
    private GameObject objectInHand;

    private SteamVR_TrackedObject trackedObj;

    // objektas kuriam galima keist textura
    public static GameObject texturedObject;

    public static GameObject objectWithRigibody;

    private SteamVR_Controller.Device Controller
    {
        get { return SteamVR_Controller.Input((int)trackedObj.index); }
    }

    void Awake()
    {
        trackedObj = GetComponent<SteamVR_TrackedObject>();
    }

    private void SetCollidingObject(Collider col)
    {
        //      Debug.Log(col.gameObject.name);
        if (!col.GetComponent<Rigidbody>())
        {
            texturedObject = col.gameObject;
        }
        else {
            objectWithRigibody = col.gameObject;
        }
        // neimti jai zaidejas jau kazka laiko raba neturi rigibody.
        if (collidingObject || !col.GetComponent<Rigidbody>())
        {
            return;
        }
        // nustato kaip potencialu paimama objekta.
        texturedObject = null;
        collidingObject = col.gameObject;
    }







    // kai ieina i trigeri
    public void OnTriggerEnter(Collider other)
    {
        SetCollidingObject(other);
    }

    // Kaip ^ tik suveikia kai trigeri pabuna kuri laika
    public void OnTriggerStay(Collider other)
    {
        SetCollidingObject(other);
    }

    // kai iseina is trigerio pasalina sugriebta objekta.
    public void OnTriggerExit(Collider other)
    {
        texturedObject = null;
        if (!collidingObject)
        {
            return;
        }

        collidingObject = null;
    }

    private void GrabObject()
    {
        // paima objekta prie kontrolerio
        objectInHand = collidingObject;
        collidingObject = null;
        // 
        var joint = AddFixedJoint();
        joint.connectedBody = objectInHand.GetComponent<Rigidbody>();
    }

    // 3
    private FixedJoint AddFixedJoint()
    {
        FixedJoint fx = gameObject.AddComponent<FixedJoint>();
        fx.breakForce = 20000;
        fx.breakTorque = 20000;
        return fx;
    }

    private void ReleaseObject()
    {
        // fixed joint ?
        if (GetComponent<FixedJoint>())
        {
            // destroy joint and connection
            GetComponent<FixedJoint>().connectedBody = null;
            Destroy(GetComponent<FixedJoint>());
            // prideda geiti ir rotacija 
            objectInHand.GetComponent<Rigidbody>().velocity = Controller.velocity;
            objectInHand.GetComponent<Rigidbody>().angularVelocity = Controller.angularVelocity;
        }
        // 4
        objectInHand = null;
    }

    // Update is called once per frame
    void Update () {
        // Kai paspaudziamas mygtukas paimamas objektas
        if (Controller.GetHairTriggerDown())
        {
            if (collidingObject)
            {
                GrabObject();
            }
        }

        // Jai atleidzia mygtuka kai pasirinktas objektas  objektas paleidziamas
        if (Controller.GetHairTriggerUp())
        {
            if (objectInHand)
            {
                ReleaseObject();
            }
        }
    }
}
