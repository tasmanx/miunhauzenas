﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CannonHandle : MonoBehaviour {
    void OnTriggerStay(Collider collider) {
        if (collider.gameObject.tag == "hand") {
            transform.parent.parent.GetComponent<CannonAim>().handTouched = true;
            //transform.parent.GetComponent<CannonAim>().SetAngleDifs();
        }
    }
    void OnTriggerEnter(Collider collider)
    {
        if (collider.gameObject.tag == "hand")
        {
            transform.parent.parent.GetComponent<CannonAim>().SetAngleDifs();
            transform.parent.parent.GetComponent<CannonAim>().SetHeightAngleDif();
        }
    }
}
