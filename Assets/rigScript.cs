﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class rigScript : MonoBehaviour {

    private bool stopFalling;

    public GameObject rightController, leftController, headController;

	// Use this for initialization
	void Start () {
        stopFalling = false;

    }
	
	// Update is called once per frame
	void Update () {
        Debug.Log("Test");
        if (!stopFalling)
        {
            Debug.Log("Test 222");
            gameObject.transform.position -= new Vector3(0, 0.09f, 0);
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        Debug.Log("OTHER:    " +  other.name);
        if (other.GetComponent<CannonBall>() == null && other.gameObject != rightController && other.gameObject != leftController && other.gameObject != headController) {
            Debug.Log("Trigger: "  + other.name);
            stopFalling = true;
        }

    }

    private void OnTriggerExit(Collider other)
    {
        if (other.GetComponent<CannonBall>() == null && other.gameObject != rightController && other.gameObject != leftController && other.gameObject != headController)
        {
            stopFalling = false;
        }
    }

}
