﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
// derive from singleton tomakesingleton:
public class LoadNextLevel : Singleton<LoadNextLevel> {
    public int currentLevel = 0;
    private IEnumerator coroutine;
    void Start() {
        coroutine = Load();
        StartLoading();
    }

    //call this from other scripts to start loading next level!
    public void StartLoading() {
        StartCoroutine(coroutine);
    }

    IEnumerator Load() {
        Debug.Log("loading");
        AsyncOperation async = SceneManager.LoadSceneAsync(currentLevel + 1);
        yield return async;
        Debug.Log("Loading complete");
        currentLevel++;
    }
}
