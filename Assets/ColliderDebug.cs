﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
public class ColliderDebug : MonoBehaviour {
    float timer;
    public float delay = 2;
    bool needRestart;
    public static bool isInAir;
    void Start() {
        isInAir = false;
    }
    void Update(){
        if (needRestart)
            timer += Time.deltaTime;
        if (timer > delay && needRestart)
            RestartLevel();
    }
    void OnCollisionEnter(Collision col) {
        //Debug.Log("collisionas: "+col.collider.gameObject.name);
        if (col.collider.gameObject.tag == "Terrain") {
            Debug.Log(col.collider.gameObject.name);
            FadeManager.FadeOut();
            needRestart = true;
        }
        if (col.collider.gameObject.layer == LayerMask.NameToLayer("Tower"))
        {
            isInAir = false;
        }
    }
    //restarting after terrain is hit
    void RestartLevel() {
        SceneManager.LoadScene(1);
        needRestart = false;
    }
}
