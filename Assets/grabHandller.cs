﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class grabHandller : MonoBehaviour
{
    private bool ballIsInCollider;
    public GameObject grabbedObject, objectInTrigger;
    public GameObject cameraRig;
    private bool followBall;
    private FadeManager fm;
    public GameObject cannon;
    Vector3 positionDif;
    public static bool flying;
    // Nustatomas i true kai trigeris nuspaustas, ir i false kai atleidziamas, reikalingas tam kad butu galima pagauti sviedini laikant nuspaudus o ne tik tada kai nuspaudi kolizijos su sviediniu metu.
    private bool allowToGrab;

    // Nuoroda i stebima objekta (kontroleri).
    private SteamVR_TrackedObject trackedObj;

    // Grazina kontrolerio inputą.
    void Awake()
    {
        trackedObj = GetComponent<SteamVR_TrackedObject>();
    }
    private SteamVR_Controller.Device Controller
    {
        get { return SteamVR_Controller.Input((int)trackedObj.index); }
    }
    
    // Use this for initialization
    void Start()
    {
        flying = false;
        ballIsInCollider = false;
        followBall = false;
        allowToGrab = false;
        fm = new FadeManager();
         cameraRig.GetComponent<Rigidbody>().useGravity = true;
    }

    void EnableRagdoll()
    {
        cameraRig.GetComponent<Rigidbody>().isKinematic = false;
        cameraRig.GetComponent<Rigidbody>().detectCollisions = true;
    }
    void DisableRagdoll()
    {
        cameraRig.GetComponent<Rigidbody>().isKinematic = true;
        cameraRig.GetComponent<Rigidbody>().detectCollisions = false;
    }

    void Update()
    {
        if (grabbedObject == null && cameraRig.GetComponent<Rigidbody>().velocity.magnitude < 0.1f)
        {
            flying = false;
        }
        else
        {
            flying = true;
        }
        // Kai laikom nuspaude trigeri leidziama sugriebti sviedini.
        if (Controller.GetHairTriggerDown()) {
            allowToGrab = true;
        }

        if (Controller.GetHairTriggerUp()) {
            allowToGrab = false;
            if (objectInTrigger != null)
            {
                //Debug.Log("exitas: " + objectInTrigger.gameObject.name + " .........................................................................................................");
                ballIsInCollider = false;
                objectInTrigger = null;
                followBall = false;
                cameraRig.GetComponent<Rigidbody>().useGravity = true;
                Debug.Log("rig  " + cameraRig.GetComponent<Rigidbody>().velocity + " ball:   " + grabbedObject.GetComponent<Rigidbody>().velocity);
                cameraRig.GetComponent<Rigidbody>().velocity = grabbedObject.GetComponent<Rigidbody>().velocity / 2;
                Debug.Log("rig  " + cameraRig.GetComponent<Rigidbody>().velocity + " ball:   " + grabbedObject.GetComponent<Rigidbody>().velocity);
            }
        }

        if (ballIsInCollider)
        {
            GrabObjects();
        }
        //Debug.Log(followBall);
        if (followBall)
        {
            cameraRig.transform.position =grabbedObject.transform.position - positionDif;
            cameraRig.GetComponent<Rigidbody>().useGravity = false;   
        }
       
    }

    private void GrabObjects()
    {
        // Jai triggeris nuspaustas ir nesilaikoma uz sviedinio
        if (allowToGrab && grabbedObject == null)
        {
            //Debug.Log("Paspaude mygtuka kol nieku neturejo rankoje");
            grabbedObject = objectInTrigger;
            followBall = true;
            cameraRig.GetComponent<Rigidbody>().useGravity = true;
        }

        // triger 
        //Debug.Log("objectInTrigger" + objectInTrigger + "grabbedObject" + grabbedObject  );
        if (!allowToGrab && grabbedObject != null)
        {
            //Debug.Log("Paspaude mygtuka kol rankoje turejo" + grabbedObject);
            //Destroy(grabbedObject.GetComponent<SphereCollider>());

            grabbedObject = null;
            objectInTrigger = null;
            followBall = false;
            ballIsInCollider = false;
            //cameraRig.transform.position = new Vector3(cameraRig.transform.position.x, cameraRig.transform.position.y, cameraRig.transform.position.z);
            cameraRig.GetComponent<Rigidbody>().useGravity = true;
          //  Destroy(cannon.GetComponent<MeshRenderer>());
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.GetComponent<CannonBall>() != null && allowToGrab)
        {
            ballIsInCollider = true;
            objectInTrigger = other.gameObject;
            positionDif = other.transform.position - cameraRig.transform.position;
        }
    }

    /*private void OnTriggerExit(Collider other)
    {
        if (objectInTrigger != null && other.gameObject == grabbedObject)
        {
            //Debug.Log("exitas: " + objectInTrigger.gameObject.name + " .........................................................................................................");
            ballIsInCollider = false;
            objectInTrigger = null;
            followBall = false;
            cameraRig.GetComponent<Rigidbody>().useGravity = true;
        }
    }*/

    
    }
