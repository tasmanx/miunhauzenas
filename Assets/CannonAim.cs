﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CannonAim : MonoBehaviour {
    public GameObject barrel;
    public Transform hand;
    public GameObject handle;
    public bool handTouched;

    public Quaternion angleDif;
    public Quaternion angleDifHeight;
    // Nuoroda i stebima objekta (kontroleri).
    public Transform contrLeft;
    public Transform contrRight;
    //upgrade
    public bool vr;
    public bool touchedX;
    public bool touchedY;
    public GameObject target;
    public float sensitivityX = 1f;
    public float sensitivityY = 1f;
    // Grazina kontrolerio inputą.
    private SteamVR_Controller.Device ControllerLeft
    { 
        get
        {
            if (vr)
                return SteamVR_Controller.Input((int)contrLeft.GetComponent<SteamVR_TrackedObject>().index);
            else return null;
        }
    }
    private SteamVR_Controller.Device ControllerRight
    {
        get {
            if (vr)
                return SteamVR_Controller.Input((int)contrRight.GetComponent<SteamVR_TrackedObject>().index);
            else return null;
        }
    }
    void Update() {

       if (Input.GetMouseButtonDown(0)) {
            SetAngleDifs();
            SetHeightAngleDif();
        }

        if (Input.GetMouseButton(0) )
        {
            TurnPlatform();
            TurnBarrel();
        }
        if (vr)
        {
            if (ControllerLeft.GetHairTriggerDown())
            {
                hand = contrLeft;
            }
            if (ControllerRight.GetHairTriggerDown())
            {
                hand = contrRight;
            }

        if ((ControllerLeft.GetHairTrigger() || ControllerRight.GetHairTrigger()) && handTouched) {
            TurnPlatform();
            TurnBarrel();
        }
        else
            handTouched = false;
        }

    }

    void TurnPlatform() {
        Vector3 targetPoint = new Vector3(hand.position.x, transform.position.y, hand.position.z) - transform.position;
        Quaternion targetRotation = Quaternion.LookRotation(-targetPoint, Vector3.up);
        transform.rotation = targetRotation * angleDif;
    }
    void TurnBarrel() {
        Vector3 targetPoint = new Vector3(barrel.transform.position.x, hand.position.y, hand.position.z) - barrel.transform.position;
  //      Debug.Log(targetPoint);
        if (targetPoint.z > 0)
        {
            targetPoint.z *= -1;
        }
        Quaternion targetRotationHeight = Quaternion.LookRotation(-targetPoint, Vector3.up);
        barrel.transform.localRotation = targetRotationHeight * angleDifHeight;
    }

    public void SetAngleDifs() {
        Vector3 targetPointHandle = new Vector3(handle.transform.position.x, transform.position.y, handle.transform.position.z) - transform.position;
        Quaternion b = Quaternion.LookRotation(targetPointHandle);
        Vector3 targetPointHand = new Vector3(hand.position.x, transform.position.y, hand.position.z) - transform.position;
        Quaternion a = Quaternion.LookRotation(targetPointHand, Vector3.up);
        angleDif = Quaternion.Inverse(a) * b;
    }
    public void SetHeightAngleDif() {
        Vector3 targetPointHandle = new Vector3(barrel.transform.position.x, handle.transform.position.y, handle.transform.position.z) - barrel.transform.position;
        Quaternion a = Quaternion.LookRotation(targetPointHandle);
        Vector3 targetPointHand = new Vector3(barrel.transform.position.x, hand.position.y, hand.position.z) - barrel.transform.position;
        Quaternion b = Quaternion.LookRotation(targetPointHand, Vector3.up);
        angleDifHeight = Quaternion.Inverse(a) * b;
    }
}
