﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ViveControllerInputTest : MonoBehaviour {

    // Nuoroda i stebima objekta (kontroleri).
    private SteamVR_TrackedObject trackedObj;

    // Grazina kontrolerio inputą.
    void Awake()
    {
        trackedObj = GetComponent<SteamVR_TrackedObject>();
    }
    private SteamVR_Controller.Device Controller
    {
        get { return SteamVR_Controller.Input((int)trackedObj.index); }
    }

    // Update is called once per frame
    void Update()
    {
        // touchpado inputas
        if (Controller.GetAxis() != Vector2.zero)
        {
           // Debug.Log(gameObject.name + Controller.GetAxis());
        }

        // triger when squeeze
        if (Controller.GetHairTriggerDown())
        {
           // Debug.Log(gameObject.name + " Trigger Press");
        }

        // triger release
        if (Controller.GetHairTriggerUp())
        {
          //  Debug.Log(gameObject.name + " Trigger Release");
        }

        // grip button
        if (Controller.GetPressDown(SteamVR_Controller.ButtonMask.Grip))
        {
           // Debug.Log(gameObject.name + " Grip Press");
        }

        // 5
        if (Controller.GetPressUp(SteamVR_Controller.ButtonMask.Grip))
        {
          //  Debug.Log(gameObject.name + " Grip Release");
        }
    }
}
